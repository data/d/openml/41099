# OpenML dataset: Stagger

https://www.openml.org/d/41099

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains three nominal attributes, namely size = {small, medium, large}, color={red, green}, and shape={circular, non-circular}. Before the first drift point, instances are labeled positive if (color = red) &and; (size = small). After this point and before the second drift, instances are classified positive if (color = green) v (shape = circular), and finally after this second drift point, instances are classified positive only if (size = medium) v (size = large).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41099) of an [OpenML dataset](https://www.openml.org/d/41099). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41099/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41099/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41099/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

